import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:myapp/blocs/blocs.dart';
import 'package:myapp/helpers/helpers.dart';
import 'package:myapp/httpService/services.dart';
import 'package:myapp/models/models.dart';
import 'package:myapp/screens/home/components/user_data_widget.dart';
import 'package:myapp/screens/home/home.dart';
import 'package:myapp/screens/navigation/navigation.dart';
import 'package:intl/intl.dart';

import '../../../constants.dart';

// ignore: must_be_immutable
class HomeBuilder extends StatefulWidget {
  User user;

  HomeBuilder({
    Key key,
    @required this.widget,
    @required this.context,
    @required this.user,
  }) : super(key: key);

  final Home widget;
  final BuildContext context;

  @override
  _HomeBuilderState createState() => _HomeBuilderState();
}

class _HomeBuilderState extends State<HomeBuilder> {
  Future<List<Post>> futureNews;
  HttpService httpService = HttpService();
  ScrollController controller = ScrollController();
  bool closeTopContainer = false;

  @override
  void initState() {
    super.initState();
    futureNews = httpService.getMyNotifications(widget.user.token);
  }

  final List<IconData> iconsStudent = [
    FontAwesomeIcons.qrcode, //Control de asistencia
    FontAwesomeIcons.exclamationCircle, //Sitios en mal estado
    FontAwesomeIcons.bookOpen, //Biblioteca
    FontAwesomeIcons.heartbeat, //Estado de salud
    FontAwesomeIcons.addressBook, //Mis clases
    FontAwesomeIcons.angleDown, //Ver mas
  ];

  final List<IconData> iconsTeacher = [
    FontAwesomeIcons.users, //Control de asistencia
    FontAwesomeIcons.exclamationCircle, //Sitios en mal estado
    FontAwesomeIcons.bookOpen, //Biblioteca
    FontAwesomeIcons.heartbeat, //Estado de salud
    FontAwesomeIcons.chalkboardTeacher, //Gestion de aulas
    FontAwesomeIcons.angleDown, //Ver mas
  ];

  final List<String> textIconsStudent = [
    "Asistencia",
    "Incidencias",
    "Biblioteca",
    "Estado de salud",
    "Mis clases",
    "Más opciones",
  ];

  final List<String> textIconsTeacher = [
    "Estudiantes",
    "Incidencias",
    "Biblioteca",
    "Estado de salud",
    "Mis asignaturas",
    "Más opciones",
  ];

  Widget _buildICon(int iconIndex) {
    return GestureDetector(
      onTap: () {
        widget.widget.user.role == 'Alumno'
            ? studentNavigation(iconIndex)
            : teacherNavigation(iconIndex);
      },
      child: Row(
        children: <Widget>[
          SizedBox(width: MediaQuery.of(widget.context).size.width / 12),
          Container(
            height: 60.0,
            width: 60.0,
            decoration: BoxDecoration(
              color: newBackground,
              borderRadius: BorderRadius.circular(30.0),
            ),
            child: Icon(
              widget.user.role == 'Alumno'
                  ? iconsStudent[iconIndex]
                  : iconsTeacher[iconIndex],
              size: 25.0,
              color: widget.user.role == 'Alumno' ? appThirdColor : iconColor,
            ),
          ),
          SizedBox(width: MediaQuery.of(widget.context).size.width / 12),
        ],
      ),
    );
  }

  Widget _buildTextICon(int indexText) {
    return Container(
      width: MediaQuery.of(widget.context).size.width / 3,
      child: Center(
        child: Text(
          widget.widget.user.role == 'Alumno'
              ? textIconsStudent[indexText]
              : textIconsTeacher[indexText],
          style: TextStyle(fontSize: 11.5, color: myBlue),
        ),
      ),
    );
  }

  void studentNavigation(int iconIndex) {
    switch (iconIndex) {
      case 0:
        Navigator.push(
            widget.context,
            MaterialPageRoute(
                builder: (context) =>
                    ScanQR(user: widget.widget.user, biblioteca: false)));
        break;
      case 1:
        Navigator.push(
            widget.context,
            MaterialPageRoute(
                builder: (context) =>
                    IncidenciasForm(user: widget.widget.user)));
        break;
      case 2:
        Navigator.push(
            widget.context,
            MaterialPageRoute(
                builder: (context) => BiblioOptions(user: widget.widget.user)));
        break;
      case 3:
        Navigator.push(
            widget.context,
            MaterialPageRoute(
                builder: (context) =>
                    UserState(token: widget.widget.user.token)));
        break;
      case 4:
        Navigator.push(
            widget.context,
            MaterialPageRoute(
                builder: (context) =>
                    StudentClasses(user: widget.widget.user)));
        break;
      case 5:
        Navigator.push(
            widget.context,
            MaterialPageRoute(
                builder: (context) => MoreOptions(user: widget.widget.user)));
        break;
      default:
        print("Opción inválida");
        break;
    }
  }

  void teacherNavigation(int iconIndex) {
    switch (iconIndex) {
      case 0:
        Navigator.push(
            widget.context,
            MaterialPageRoute(
                builder: (context) => StudentList(user: widget.widget.user)));
        break;
      case 1:
        Navigator.push(
            widget.context,
            MaterialPageRoute(
                builder: (context) =>
                    IncidenciasForm(user: widget.widget.user)));
        break;
      case 2:
        _showcontent();
        break;
      case 3:
        Navigator.push(
            widget.context,
            MaterialPageRoute(
                builder: (context) =>
                    UserState(token: widget.widget.user.token)));
        break;
      case 4:
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SubjectList(user: widget.user)));
        break;
      case 5:
        _showcontent();
        break;
      default:
        print("Opción inválida");
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final authBloc = BlocProvider.of<AuthenticationBloc>(context);
    final user = widget.widget.user;
    final Size size = MediaQuery.of(context).size;
    final double categoryHeight = size.height * 0.27;

    List<Post> news = new List<Post>();

    return Expanded(
      child: Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 10.0),
            Container(
              margin: EdgeInsets.only(right: 10, left: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Bienvenido',
                    style: TextStyle(
                      color: appPrimaryColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 27,
                    ),
                  ),
                  IconButton(
                    icon: new Icon(
                      FontAwesomeIcons.signOutAlt,
                      color: appSecondaryColor,
                      size: 23.0,
                    ),
                    onPressed: () {
                      authBloc.add(UserLoggedOut());
                    },
                  ),
                ],
              ),
            ),
            SizedBox(height: 5.0),
            UserDataContainer(user: user),
            AnimatedOpacity(
              duration: const Duration(milliseconds: 300),
              opacity: closeTopContainer ? 0 : 1,
              child: AnimatedContainer(
                duration: const Duration(milliseconds: 300),
                width: size.width,
                height: closeTopContainer ? 0 : categoryHeight,
                alignment: Alignment.topCenter,
                child: buildIconsContainer(),
              ),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 25),
                decoration: BoxDecoration(
                  color: newBackground,
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(34),
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 5, right: 25, left: 25),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Noticias recientes:",
                            style: TextStyle(
                              color: myBlue,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              bool scroll = false;
                              closeTopContainer
                                  ? scroll = false
                                  : scroll = true;
                              setState(() {
                                closeTopContainer = scroll;
                              });
                            },
                            child: Text(
                              closeTopContainer ? "Ver menos" : "Ver más",
                              style: TextStyle(
                                color: widget.user.role == 'Alumno'
                                    ? appThirdColor
                                    : iconColor,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: FutureBuilder(
                        future: futureNews,
                        // ignore: missing_return
                        builder: (BuildContext context,
                            AsyncSnapshot<List<Post>> snapshot) {
                          switch (snapshot.connectionState) {
                            case ConnectionState.none:
                            case ConnectionState.active:
                            case ConnectionState.waiting:
                              return Center(child: CircularProgressIndicator());
                            case ConnectionState.done:
                              if (snapshot.hasError)
                                return Text('Error: ${snapshot.error}');

                              for (Post noticia in snapshot.data) {
                                if (noticia.type == "NOTICIAS")
                                  news.add(noticia);
                              }
                              news.sort((postA, postB) => postB.creationDate
                                  .compareTo(postA.creationDate));

                              if (news.isNotEmpty) {
                                return Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 5.0),
                                  child: ListView(
                                    children: <Widget>[
                                      for (Post noticia in news)
                                        if (noticia.type == "NOTICIAS")
                                          cardNews(noticia),
                                    ],
                                  ),
                                );
                              } else
                                return Center(
                                  child: Text(
                                    "No hay noticias actualmente",
                                    style: TextStyle(
                                      color: appSecondaryColor,
                                      fontSize: 20.0,
                                    ),
                                  ),
                                );
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container buildIconsContainer() {
    return Container(
      child: FittedBox(
        fit: BoxFit.cover,
        alignment: Alignment.topCenter,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 20, bottom: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  for (int i = 0;
                      i <
                          (widget.widget.user.role == 'Alumno'
                                  ? iconsStudent.length
                                  : iconsTeacher.length) /
                              2;
                      i++)
                    _buildICon(i),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                for (int i = 0;
                    i <
                        (widget.widget.user.role == 'Alumno'
                                ? iconsStudent.length
                                : iconsTeacher.length) /
                            2;
                    i++)
                  _buildTextICon(i)
              ],
            ),
            SizedBox(height: 15.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                for (int i = 3;
                    i <
                        (widget.widget.user.role == 'Alumno'
                            ? iconsStudent.length
                            : iconsTeacher.length);
                    i++)
                  _buildICon(i)
              ],
            ),
            SizedBox(height: 5.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                for (int i = 3;
                    i <
                        (widget.widget.user.role == 'Alumno'
                            ? iconsStudent.length
                            : iconsTeacher.length);
                    i++)
                  _buildTextICon(i)
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget cardNews(Post noticia) {
    return GestureDetector(
      onTap: () {},
      child: Padding(
        padding: const EdgeInsets.only(right: 10.0, left: 10.0),
        child: Card(
          //color: Colors.white70,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 5.0,
          child: Padding(
            padding: const EdgeInsets.all(3.0),
            child: ListTile(
              title: Row(
                children: <Widget>[
                  Container(
                      width: 30.0,
                      height: 30.0,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(60 / 2),
                      ),
                      child: Icon(
                        FontAwesomeIcons.solidNewspaper,
                        size: 25.0,
                        color: widget.user.role == 'Alumno'
                            ? appThirdColor
                            : iconColor,
                      )),
                  SizedBox(width: 15.0),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          noticia.title,
                          style: TextStyle(
                            fontSize: 16.0,
                            color: appSecondaryColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 5.0),
                        Text(
                          noticia.description,
                          style: TextStyle(
                            fontSize: 14.0,
                            color: Colors.grey[600],
                          ),
                        ),
                        SizedBox(height: 7.0),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Text(
                            getTimeAndData(noticia.creationDate),
                            style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.grey[500],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _showcontent() {
    showDialog(
      context: context, barrierDismissible: false, // user must tap button!

      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('Alerta !'),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: [
                new Text('Aún no hay función asignada'),
              ],
            ),
          ),
          actions: [
            new FlatButton(
              child: new Text('Ok'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  String getTimeAndData(DateTime date) {
    String month = DateFormat('MMMM').format(date);
    String numDay = DateFormat('d').format(date);
    String year = DateFormat('y').format(date);
    month = ConversionHelper().spanishMonth(month);
    return numDay + ' ' + month + ' ' + year;
  }
}
