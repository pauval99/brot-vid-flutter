import 'dart:convert';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:myapp/httpService/services.dart';
import 'package:myapp/models/models.dart';

import '../../../constants.dart';

class ScanQR extends StatefulWidget {
  final User user;
  final bool biblioteca;

  const ScanQR({Key key, this.user, this.biblioteca}) : super(key: key);

  @override
  _ScanQRState createState() => _ScanQRState();
}

class _ScanQRState extends State<ScanQR> {
  String qrCodeResult = "Ningún sitio seleccionado";

  bool asigned = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 6),
              child: IconButton(
                icon: const Icon(
                  FontAwesomeIcons.angleLeft,
                  color: appSecondaryColor,
                  size: 30.0,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            );
          },
        ),
        title: Text(
          widget.biblioteca ? 'Reservar' : 'Notificar asistencia',
          style: TextStyle(
            color: appPrimaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 22,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              widget.biblioteca
                  ? "Sitio seleccionado:"
                  : "Asiento seleccionado:",
              style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 15.0,
            ),
            Text(
              qrCodeResult,
              style: TextStyle(
                fontSize: 20.0,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 40.0,
            ),
            FlatButton(
              padding: EdgeInsets.all(15.0),
              onPressed: () async {
                String codeScanner = await BarcodeScanner.scan();
                setState(() {
                  qrCodeResult = codeScanner;
                });
                asigned = true;
              },
              child: Text(
                "ABRIR ESCANER",
                style: TextStyle(
                  color: appSecondaryColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0,
                ),
              ),
              shape: RoundedRectangleBorder(
                side: BorderSide(
                  color: appPrimaryColor,
                  width: 3.0,
                ),
                borderRadius: BorderRadius.circular(20.0),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            if (asigned)
              FlatButton(
                padding: EdgeInsets.all(15.0),
                onPressed: () async {
                  if (!widget.biblioteca) {
                    http.Response response = await httpService.readQR(
                        widget.user.token, qrCodeResult);
                    response.statusCode == 201
                        ? buildShowDialog(
                            context, json.decode(response.body), true)
                        : buildShowDialog(
                            context, json.decode(response.body), false);
                  } else {
                    http.Response response = await httpService.readQRbiblio(
                        widget.user.token, qrCodeResult);
                    response.statusCode == 201
                        ? buildShowDialog(
                            context, json.decode(response.body), true)
                        : buildShowDialog(
                            context, json.decode(response.body), false);
                  }
                },
                child: Text(
                  widget.biblioteca ? "ASGINAR SITIO" : "ASIGNAR ASIENTO",
                  style: TextStyle(
                    color: appSecondaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0,
                  ),
                ),
                shape: RoundedRectangleBorder(
                    side: BorderSide(color: appPrimaryColor, width: 3.0),
                    borderRadius: BorderRadius.circular(20.0)),
              )
          ],
        ),
      ),
    );
  }

  Future buildShowDialog(BuildContext context, datajson, bool success) {
    return showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
          child: dialogContent(context, datajson["message"], success),
        );
      },
    );
  }

  dialogContent(BuildContext context, String message, bool success) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: 85, bottom: 16, left: 16, right: 16),
          margin: EdgeInsets.only(top: 37),
          decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(17),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: Offset(10.0, 20.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                success ? "Acción exitosa !" : "Lo sentimos !",
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
              SizedBox(height: 24.0),
              Text(
                message.toUpperCase(),
                style: TextStyle(fontSize: 16.0),
              ),
              SizedBox(height: 24.0),
              Align(
                alignment: Alignment.bottomRight,
                child: FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Confirmar"),
                ),
              )
            ],
          ),
        ),
        Positioned(
          top: 0,
          left: 16.0,
          right: 16.0,
          child: CircleAvatar(
            backgroundColor: newBackground,
            radius: 50,
            child: Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Icon(
                success ? FontAwesomeIcons.checkDouble : FontAwesomeIcons.bomb,
                size: 45.0,
                color: success ? myGreen : myRed,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
