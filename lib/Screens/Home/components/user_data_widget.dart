import 'package:flutter/material.dart';
import 'package:myapp/models/models.dart';

import '../../../constants.dart';

class UserDataContainer extends StatelessWidget {
  const UserDataContainer({
    Key key,
    @required this.user,
  }) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) {
    var mywidht = MediaQuery.of(context).size.width - 35;
    return Container(
      height: 80.0,
      width: mywidht,
      decoration: BoxDecoration(
        color: user.role == "Alumno" ? appThirdColor : appSecondaryColor,
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(width: mywidht / 13),
          Container(
            height: 60.0,
            width: 60.0,
            decoration: BoxDecoration(
              color: newBackground,
              borderRadius: BorderRadius.circular(30.0),
              image: DecorationImage(
                image: AssetImage('assets/icons/profile.png'),
              ),
            ),
          ),
          SizedBox(width: mywidht / 15),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 17.0),
              Text(
                '${user.name}',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 3.0),
              Text(
                '${user.role}',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  // fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
