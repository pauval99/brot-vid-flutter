import 'package:flutter/material.dart';

const appPrimaryColor = Color(0xFF00caf2); //lightblue
const appSecondaryColor = Color(0xFF003951); //darkblue
const appThirdColor = Color(0xFFFF5D00);
const newBackground = Color(0xFFE7ECEF);

const myGreen = Color(0xFF05DC6A);
const myRed = Color(0xFFF20023);

const myBlue = Color(0xFF011826);
const textFieldColor = Color(0xFFEDF5F7);
const iconBackgorundColor = Color(0xFFD8ECF1);
const iconColor = Color(0xFF3EBACE);
const darkRed = Color(0xFF9d0208);
const carrotOrange = Color(0xFFFFBB00);
const lightOrange = Color(0xFFE67000);
const prova = Color(0xFF089499);

const MaterialColor myPrimaryColor = MaterialColor(0xFF003951, color);

const Map<int, Color> color = {
  50: Color(0xFF00caf2),
  100: Color(0xFF00caf2),
  200: Color(0xFF00caf2),
  300: Color(0xFF00caf2),
  400: Color(0xFF00caf2),
  500: Color(0xFF00caf2),
  600: Color(0xFF00caf2),
  700: Color(0xFF00caf2),
  800: Color(0xFF00caf2),
  900: Color(0xFF00caf2),
};
