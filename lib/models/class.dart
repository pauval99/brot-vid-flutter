import 'package:equatable/equatable.dart';

class Class extends Equatable {
  final String subject;
  final String room;
  final String weekday;
  // ignore: non_constant_identifier_names
  final int h_ini;
  // ignore: non_constant_identifier_names
  final int h_end;
  final String teacher;
  // ignore: non_constant_identifier_names
  final String mail_teacher;
  // ignore: non_constant_identifier_names
  final int num_infected;
  final bool attendance;

  const Class({
    this.subject,
    this.room,
    this.weekday,
    // ignore: non_constant_identifier_names
    this.h_ini,
    // ignore: non_constant_identifier_names
    this.h_end,
    this.teacher,
    // ignore: non_constant_identifier_names
    this.mail_teacher,
    // ignore: non_constant_identifier_names
    this.num_infected,
    this.attendance,
  });

  @override
  List<Object> get props => [
        subject,
        room,
        weekday,
        h_ini,
        h_end,
        teacher,
        mail_teacher,
        num_infected
      ];

  @override
  String toString() =>
      'Class { subject: $subject, room: $room, weekday: $weekday, h_ini: $h_ini, h_end: $h_end, teacher: $teacher, mail_teacher: $mail_teacher, num_infected: $num_infected }';
}
