import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:myapp/httpService/services.dart';
import 'package:myapp/models/models.dart';
import 'package:intl/intl.dart';

import '../../../constants.dart';

class StudentHistory extends StatefulWidget {
  final User user;
  final String token;

  const StudentHistory({Key key, this.user, this.token}) : super(key: key);

  @override
  _StudentHistoryState createState() => _StudentHistoryState();
}

class _StudentHistoryState extends State<StudentHistory> {
  Future<List<Seat>> future;
  HttpService httpService = HttpService();

  @override
  void initState() {
    super.initState();
    future = httpService.getUserSeatHistory(widget.token, widget.user.email);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 6),
              child: IconButton(
                icon: const Icon(
                  FontAwesomeIcons.angleLeft,
                  color: appSecondaryColor,
                  size: 30.0,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            );
          },
        ),
        title: Text(
          'Historial de asientos',
          style: TextStyle(
            color: appPrimaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 22,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return FutureBuilder(
      future: future,
      // ignore: missing_return
      builder: (BuildContext context, AsyncSnapshot<List<Seat>> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');

            if (snapshot.data.isNotEmpty)
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: ListView(
                  children: <Widget>[
                    for (Seat seat in snapshot.data) _seatCard(seat),
                  ],
                ),
              );
            else
              return Center(
                child: Text(
                  "Historial actualmente vacío",
                  style: TextStyle(
                    color: appSecondaryColor,
                    fontSize: 20.0,
                  ),
                ),
              );
        }
      },
    );
  }

  Widget _seatCard(Seat seat) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
        child: ListTile(
          title: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: buildColumnRoom(seat),
              ),
              SizedBox(width: 15.0),
              Expanded(
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text(
                                    seat.teacher,
                                    style: TextStyle(
                                      color: appSecondaryColor,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                    seat.subject,
                                    style: TextStyle(
                                      color: appSecondaryColor,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Silla: ",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: appSecondaryColor,
                                    ),
                                  ),
                                  Text(
                                    seat.chair,
                                    style: TextStyle(
                                      color: appSecondaryColor,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Inicio: ",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: appSecondaryColor,
                                    ),
                                  ),
                                  Text(
                                    seat.h_ini.toString() + ":00",
                                    style: TextStyle(
                                      color: appSecondaryColor,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Fin: ",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: appSecondaryColor,
                                    ),
                                  ),
                                  Text(
                                    seat.h_end.toString() + ":00",
                                    style: TextStyle(
                                      color: appSecondaryColor,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 20.0)
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 10.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            child: Icon(
                              FontAwesomeIcons.solidClock,
                              color: Colors.grey,
                              size: 12.0,
                            ),
                          ),
                          SizedBox(width: 7.0),
                          Text(
                            getTimeAndData(seat.date),
                            style: TextStyle(
                              fontSize: 15.0,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Column buildColumnRoom(Seat seat) {
    return Column(
      children: <Widget>[
        Text(
          seat.building,
          style: TextStyle(
            fontSize: 23.0,
            fontWeight: FontWeight.bold,
            color: appThirdColor,
          ),
        ),
        Text(
          seat.floor.toString() + "0" + seat.number.toString(),
          style: TextStyle(fontSize: 18.0),
        ),
      ],
    );
  }

  String getTimeAndData(DateTime date) =>
      DateFormat('yyyy-MM-dd - hh:mm').format(date);
}
