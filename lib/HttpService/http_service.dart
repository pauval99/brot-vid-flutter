import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'dart:core';
import 'package:http/http.dart' as http;
import 'package:myapp/models/models.dart';
import '../helpers/helpers.dart';

class HttpService {
  final String apiUrl = "http://35.180.26.95:3000/api/";

  Future<int> signIn(String email, String password) async {
    if (email == null || password == null) {
      return 444;
    }
    String request = "signin";
    http.Response response = await http
        .post(apiUrl + request, body: {"email": email, "password": password});
    if (response.statusCode == 200) {
      await saveCredentials(response);
    }
    return response.statusCode;
  }

  Future saveCredentials(http.Response response) async {
    var res = jsonDecode(response.body);
    await SharedPreferencesHelper.setToken(res["token"].toString());
  }

  Future<User> getUser(String token) async {
    final String request = 'getuser';
    http.Response response = await http.get(apiUrl + request, headers: {
      'Authorization': 'Bearer $token',
    });
    return ConversionHelper().toCurrentUser(response, token);
  }

  Future<int> addPositive(String token) async {
    final String request = 'home/addpositive';
    http.Response response = await http.put(apiUrl + request, headers: {
      HttpHeaders.authorizationHeader: "Bearer $token",
    });
    print(jsonDecode(response.body));
    return response.statusCode;
  }

  Future<int> addNegative(String token) async {
    final String request = 'home/addnegative';
    http.Response response = await http.put(apiUrl + request, headers: {
      'Authorization': 'Bearer $token',
    });
    print(jsonDecode(response.body));
    return response.statusCode;
  }

  Future<int> addConfined(String token) async {
    final String request = 'home/addconfined';
    http.Response response = await http.put(apiUrl + request, headers: {
      'Authorization': 'Bearer $token',
    });
    print(jsonDecode(response.body));
    return response.statusCode;
  }

  Future<int> addDesconfined(String token) async {
    final String request = 'home/adddesconfined';
    http.Response response = await http.put(apiUrl + request, headers: {
      'Authorization': 'Bearer $token',
    });
    print(jsonDecode(response.body));
    return response.statusCode;
  }

  Future<http.Response> readQR(String token, String qr) async {
    final String request = 'home/readQR';
    http.Response response = await http.put(apiUrl + request, headers: {
      HttpHeaders.authorizationHeader: "Bearer $token",
    }, body: {
      "qr": qr,
    });
    return response;
  }

  Future<http.Response> readQRbiblio(String token, String qr) async {
    final String request = 'home/readQRbiblio';
    http.Response response = await http.put(apiUrl + request, headers: {
      HttpHeaders.authorizationHeader: "Bearer $token",
    }, body: {
      "qr": qr,
    });
    return response;
  }

  Future<http.Response> getAvailableChairs(String token, String floor) async {
    final String request = 'availablechairs?floor=' + floor;
    http.Response response = await http.get(apiUrl + request, headers: {
      'Authorization': 'Bearer $token',
    });
    if (response.statusCode == 200) {
      return response;
    } else {
      throw Exception('No se han podido cargar las sillas disponibles');
    }
  }

  Future<List<Post>> getMyNotifications(String token) async {
    final String request = 'mynotifications';
    http.Response response = await http.get(apiUrl + request, headers: {
      'Authorization': 'Bearer $token',
    });
    if (response.statusCode == 200) {
      final datajson = json.decode(response.body);
      final data = datajson["message"] as List;
      return data.map((rawPost) {
        return ConversionHelper().toNotificationModel(rawPost);
      }).toList();
    } else if (response.statusCode == 404) {
      return List<Post>();
    } else {
      throw Exception('No se han podido cargar las notificaciones');
    }
  }

  Future<List<User>> getAllStudents(String token) async {
    final String request = 'users';
    http.Response response = await http.get(apiUrl + request, headers: {
      'Authorization': 'Bearer $token',
    });
    if (response.statusCode == 200) {
      final datajson = json.decode(response.body);
      final data = datajson["message"] as List;
      return data.map((rawPost) {
        return ConversionHelper().toUserModel(rawPost);
      }).toList();
    } else if (response.statusCode == 404) {
      return List<User>();
    } else {
      throw Exception('No se han podido cargar los Usuarios');
    }
  }

  Future<List<Seat>> getUserSeatHistory(String token, String email) async {
    final String request = 'seathistory?email=' + email;
    http.Response response = await http.get(apiUrl + request, headers: {
      'Authorization': 'Bearer $token',
    });
    List<Seat> listOfSeats;
    if (response.statusCode == 200) {
      final datajson = json.decode(response.body) as List;
      if (datajson.isNotEmpty) {
        listOfSeats = datajson.map((seatJson) {
          return ConversionHelper().toSeatModel(seatJson);
        }).toList();
        listOfSeats.sort((a, b) => b.date.compareTo(a.date));
        return listOfSeats;
      }
      return List<Seat>();
    } else {
      throw Exception('No se han podido cargar los Usuarios');
    }
  }

  Future<List<Class>> getUserClasses(String token) async {
    final String request = 'getuserclasses';
    http.Response response = await http.get(apiUrl + request, headers: {
      'Authorization': 'Bearer $token',
    });
    List<Class> listOfClasses;
    if (response.statusCode == 200) {
      final datajson = json.decode(response.body) as List;
      if (datajson.isNotEmpty) {
        listOfClasses = datajson.map((classJson) {
          return ConversionHelper().toClassModel(classJson);
        }).toList();
        return listOfClasses;
      }
      return List<Class>();
    } else {
      throw Exception('No se han podido cargar las clases');
    }
  }

  Future<List<Class>> getUserClassesByID(String token, String id) async {
    final String request = 'subjectsuser/' + id;
    http.Response response = await http.get(apiUrl + request, headers: {
      'Authorization': 'Bearer $token',
    });
    List<Class> listOfClasses;
    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      final datajson = data["subjs"] as List;
      if (datajson.isNotEmpty) {
        listOfClasses = datajson.map((classJson) {
          return Class(
            subject: classJson["name"],
            attendance: classJson['attendance'],
          );
        }).toList();
        return listOfClasses;
      }
      return List<Class>();
    } else {
      throw Exception('No se han podido cargar las clases');
    }
  }

  Future<List<Room>> getAvailableRooms(String token) async {
    final String request = 'availablerooms';
    http.Response response = await http.get(apiUrl + request, headers: {
      'Authorization': 'Bearer $token',
    });
    if (response.statusCode == 200) {
      final datajson = json.decode(response.body);
      final data = datajson["message"] as List;
      return data.map((rawPost) {
        return ConversionHelper().toRoomModel(rawPost);
      }).toList();
    } else if (response.statusCode == 404) {
      return List<Room>();
    } else {
      throw Exception('No se han podido cargar las aulas disponibles');
    }
  }

  Future<List<Subject>> getSubjects(String token) async {
    final String request = 'subjectsteacher';
    http.Response response = await http.get(apiUrl + request, headers: {
      'Authorization': 'Bearer $token',
    });
    List<Subject> listOfSubjects;
    if (response.statusCode == 200) {
      final datajson = json.decode(response.body) as List;
      if (datajson.isNotEmpty) {
        listOfSubjects = datajson.map((classJson) {
          return ConversionHelper().toSubjectModel(classJson);
        }).toList();
        return listOfSubjects;
      }
      return List<Subject>();
    } else {
      throw Exception('No se han podido cargar las asiganturas');
    }
  }

  Future<http.Response> changeDocentGuide(String token, int codeSubject) async {
    final String request = 'subject/docentguide/' + codeSubject.toString();
    http.Response response = await http.put(apiUrl + request, headers: {
      'Authorization': "Bearer $token",
    });
    if (response.statusCode == 404) {
      throw Exception('No hay alumnos matriculados');
    } else if (response.statusCode == 500) {
      throw Exception('Peto');
    } else {
      return response;
    }
  }

  Future<http.Response> changeAssistanceTypeOnline(
      String token, int codeSubject) async {
    final String request = 'subject/assistanceonline/' + codeSubject.toString();
    http.Response response = await http.put(apiUrl + request, headers: {
      'Authorization': "Bearer $token",
    });
    return response;
  }

  Future<http.Response> changeAssistanceTypeF2F(
      String token, int codeSubject) async {
    final String request = 'subject/assistance/' + codeSubject.toString();
    http.Response response = await http.put(apiUrl + request, headers: {
      'Authorization': 'Bearer $token',
    });
    return response;
  }

  Future<http.Response> addIncidences(
      String token, String title, String description) async {
    final String request = 'addincidences';
    http.Response response = await http.post(apiUrl + request, body: {
      "title": title,
      "descripcion": description
    }, headers: {
      'Authorization': 'Bearer $token',
    });
    return response;
  }

  logOut() async {
    await SharedPreferencesHelper.deleteCredentials();
  }
}
