import 'package:flutter/material.dart';

import '../../constants.dart';

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 1),
      width: size.width * 0.85,
      decoration: BoxDecoration(
        color: textFieldColor,
        borderRadius: BorderRadius.circular(20),
      ),
      child: child,
    );
  }
}
