import 'package:meta/meta.dart';

class User {
  final String id;
  final String name;
  final String role;
  final String email;
  final String token;
  final bool confined;
  final bool infected;
  final String signupDate;

  User({
    this.id,
    @required this.name,
    @required this.role,
    @required this.email,
    this.token,
    @required this.confined,
    @required this.infected,
    @required this.signupDate,
  });

  @override
  String toString() =>
      'User { id: $id, name: $name, role: $role, email: $email, token: $token, confined: $confined, infected: $infected, signupDate: $signupDate }';
}
