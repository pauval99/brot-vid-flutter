import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:myapp/helpers/helpers.dart';
import 'package:http/http.dart' as http;
import 'package:myapp/httpService/services.dart';
import 'package:myapp/models/models.dart';

import '../../../constants.dart';

class SubjectList extends StatefulWidget {
  final User user;

  const SubjectList({Key key, @required this.user}) : super(key: key);

  @override
  _SubjectListState createState() => _SubjectListState();
}

class _SubjectListState extends State<SubjectList> {
  Future<List<Subject>> futureClasses;
  HttpService httpService = HttpService();
  Subject subject = new Subject();

  @override
  void initState() {
    super.initState();
    futureClasses = httpService.getSubjects(widget.user.token);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 6),
              child: IconButton(
                icon: const Icon(
                  FontAwesomeIcons.angleLeft,
                  color: appSecondaryColor,
                  size: 30.0,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            );
          },
        ),
        title: Text(
          'Mis asignaturas',
          style: TextStyle(
            color: appPrimaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 22,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      endDrawer: Padding(
        padding: const EdgeInsets.only(left: 120.0),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25),
            bottomLeft: Radius.circular(25),
          ),
          child: Drawer(
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                Container(
                  height: size.height / 3,
                  child: DrawerHeader(
                    margin: EdgeInsets.zero,
                    padding: EdgeInsets.zero,
                    decoration: BoxDecoration(color: appThirdColor),
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          bottom: 12.0,
                          left: 16.0,
                          child: Text(
                            "Cambio de estado: ",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                ListTile(
                  leading: Icon(FontAwesomeIcons.history, size: 22.0),
                  title: Text('Cambio en la guía docente'),
                  onTap: () async {
                    Navigator.pop(context);
                    http.Response response =
                        await httpService.changeDocentGuide(
                            "${widget.user.token}", subject.code);
                    response.statusCode == 200
                        ? DialogHelper().buildShowDialog(
                            context, json.decode(response.body), true)
                        : DialogHelper().buildShowDialog(
                            context, json.decode(response.body), false);
                  },
                ),
                ListTile(
                  leading: Icon(Icons.class__rounded),
                  title: Text('Cambiar tipo de asistencia'),
                  onTap: () async {
                    Navigator.pop(context);
                    if (subject.attendance) {
                      http.Response response =
                          await httpService.changeAssistanceTypeF2F(
                              "${widget.user.token}", subject.code);
                      response.statusCode == 200
                          ? DialogHelper().buildShowDialog(
                              context, json.decode(response.body), true)
                          : DialogHelper().buildShowDialog(
                              context, json.decode(response.body), false);
                    } else {
                      http.Response response =
                          await httpService.changeAssistanceTypeOnline(
                              "${widget.user.token}", subject.code);
                      response.statusCode == 200
                          ? DialogHelper().buildShowDialog(
                              context, json.decode(response.body), true)
                          : DialogHelper().buildShowDialog(
                              context, json.decode(response.body), false);
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return FutureBuilder(
      future: futureClasses,
      // ignore: missing_return
      builder: (BuildContext context, AsyncSnapshot<List<Subject>> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');

            if (snapshot.data.isNotEmpty)
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: ListView(
                  children: <Widget>[
                    for (Subject upcClass in snapshot.data)
                      _seatCard(upcClass, context),
                  ],
                ),
              );
            else
              return Center(
                child: Text(
                  "No impartes ninguna asignatura.",
                  style: TextStyle(
                    color: appSecondaryColor,
                    fontSize: 20.0,
                  ),
                ),
              );
        }
      },
    );
  }

  Widget _seatCard(Subject upcClass, BuildContext context) {
    return Card(
      child: Padding(
        padding: EdgeInsets.only(left: 15.0, top: 5.0, bottom: 5.0),
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Text(
                    upcClass.name,
                    style: TextStyle(
                      fontSize: 23.0,
                      fontWeight: FontWeight.bold,
                      color: appThirdColor,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(width: 13),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                "Aula: ",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: appSecondaryColor,
                                  fontSize: 16.0,
                                ),
                              ),
                              Text(
                                upcClass.room,
                                style: TextStyle(
                                  color: appSecondaryColor,
                                  fontSize: 16.0,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Text(
                                "Asistencia: ",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: appSecondaryColor,
                                  fontSize: 16.0,
                                ),
                              ),
                              Text(
                                upcClass.presencialidad,
                                style: TextStyle(
                                  color: appSecondaryColor,
                                  fontSize: 16.0,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.centerRight,
                child: IconButton(
                  icon: Icon(
                    FontAwesomeIcons.ellipsisV,
                    color: appSecondaryColor,
                    size: 22.0,
                  ),
                  onPressed: () {
                    setState(() {
                      subject = upcClass;
                    });
                    Scaffold.of(context).openEndDrawer();
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Row rowGenerator(String title, String content, bool confined) {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 1.0),
          child: Text(
            title,
            style: TextStyle(
              fontSize: 17.0,
              fontWeight: FontWeight.w700,
              color: confined ? appThirdColor : myBlue,
            ),
          ),
        ),
        Text(
          content,
          style: TextStyle(
            fontSize: 16.0,
            color: appSecondaryColor,
          ),
        ),
      ],
    );
  }
}
