import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myapp/screens/global_widgets/rounded_button.dart';

import '../../blocs/blocs.dart';
import '../../httpService/services.dart';
import '../../constants.dart';
import 'components/background.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
          SafeArea(child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          // ignore: close_sinks
          final authBloc = BlocProvider.of<AuthenticationBloc>(context);
          if (state is AuthenticationNotAuthenticated) {
            return _AuthForm(); // show authentication form
          }
          if (state is AuthenticationFailure) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(state.message),
                  RoundedButton(
                    text: "Reintentar",
                    press: () async {
                      authBloc.add(AppLoaded());
                      SharedPreferencesHelper.deleteCredentials();
                    },
                  ),
                ],
              ),
            );
          }
          return Center(
            child: CircularProgressIndicator(
              strokeWidth: 2,
            ),
          );
        },
      )),
    );
  }
}

class _AuthForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authService = RepositoryProvider.of<AuthenticationService>(context);
    // ignore: close_sinks
    final authBloc = BlocProvider.of<AuthenticationBloc>(context);

    return Container(
      alignment: Alignment.center,
      child: BlocProvider<LoginBloc>(
        create: (context) => LoginBloc(authBloc, authService),
        child: _SignInForm(),
      ),
    );
  }
}

class _SignInForm extends StatefulWidget {
  @override
  __SignInFormState createState() => __SignInFormState();
}

class __SignInFormState extends State<_SignInForm> {
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  final _passwordController = TextEditingController();
  final _emailController = TextEditingController();
  bool _autoValidate = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // ignore: close_sinks
    final _loginBloc = BlocProvider.of<LoginBloc>(context);

    _onLoginButtonPressed() {
      if (_key.currentState.validate()) {
        _loginBloc.add(LoginInWithEmailButtonPressed(
            email: _emailController.text, password: _passwordController.text));
      } else {
        setState(() {
          _autoValidate = true;
        });
      }
    }

    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginFailure) {
          _showError(state.error);
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          if (state is LoginLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return Form(
            key: _key,
            // ignore: deprecated_member_use
            autovalidate: _autoValidate,
            child: Background(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: size.height * 0.04),
                    Image.asset(
                      'assets/images/LOGO.png',
                      width: 235,
                      height: 235,
                    ),
                    buildTextContainer("Bienvenido a", 0.0),
                    buildTextContainer("Brot-Vid", 25.0),
                    EmailField(
                      size: size,
                      emailController: _emailController,
                    ),
                    PasswordField(
                      size: size,
                      passwordController: _passwordController,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 20),
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 4),
                      width: size.width * 0.6,
                      child: RaisedButton(
                        color: Theme.of(context).primaryColor,
                        textColor: Colors.white,
                        padding: const EdgeInsets.all(16),
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(15)),
                        child: Text('Entrar', style: TextStyle(fontSize: 17)),
                        onPressed: state is LoginLoading
                            ? () {}
                            : _onLoginButtonPressed,
                      ),
                    ),
                    SizedBox(height: size.height * 0.07),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Container buildTextContainer(String text, double bottom) {
    return Container(
      margin: EdgeInsets.only(top: 0, bottom: bottom),
      child: Text(
        text,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.blueGrey[900],
          fontSize: 25,
        ),
      ),
    );
  }

  void _showError(String error) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(error),
      backgroundColor: appPrimaryColor,
    ));
  }
}

class EmailField extends StatelessWidget {
  const EmailField({
    Key key,
    @required this.size,
    @required TextEditingController emailController,
  })  : _emailController = emailController,
        super(key: key);

  final Size size;
  final TextEditingController _emailController;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 3),
      width: size.width * 0.85,
      height: size.height * 0.08,
      decoration: BoxDecoration(
        color: textFieldColor,
        borderRadius: BorderRadius.circular(15),
      ),
      child: TextFormField(
        decoration: InputDecoration(
          errorStyle: TextStyle(
            fontSize: 14.0,
            color: appThirdColor,
          ),
          labelText: 'Email acceso',
          border: InputBorder.none,
          prefixIcon: Padding(
            padding: EdgeInsets.only(), // add padding to adjust icon
            child: Icon(
              Icons.mail,
              color: appSecondaryColor,
            ),
          ),
        ),
        controller: _emailController,
        keyboardType: TextInputType.emailAddress,
        autocorrect: false,
        validator: (value) {
          if (value.isEmpty) {
            return 'Tienes que agregar tu email';
          }
          return null;
        },
      ),
    );
  }
}

class PasswordField extends StatefulWidget {
  const PasswordField({
    Key key,
    @required this.size,
    @required TextEditingController passwordController,
  })  : _passwordController = passwordController,
        super(key: key);

  final Size size;
  final TextEditingController _passwordController;

  @override
  _PasswordFieldState createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  bool obscureText = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 3),
      width: widget.size.width * 0.85,
      height: widget.size.height * 0.08,
      decoration: BoxDecoration(
        color: textFieldColor,
        borderRadius: BorderRadius.circular(15),
      ),
      child: TextFormField(
        decoration: InputDecoration(
          errorStyle: TextStyle(
            fontSize: 14.0,
            color: appThirdColor,
          ),
          labelText: 'Contraseña',
          border: InputBorder.none,
          prefixIcon: Padding(
            padding: EdgeInsets.only(), // add padding to adjust icon
            child: Icon(
              Icons.lock,
              color: appSecondaryColor,
            ),
          ),
          suffixIcon: IconButton(
            icon: Icon(Icons.visibility),
            onPressed: () {
              setState(() {
                obscureText = !obscureText;
              });
            },
          ),
        ),
        obscureText: obscureText,
        controller: widget._passwordController,
        validator: (value) {
          if (value.isEmpty) {
            return 'Tienes que ingresar tu contraseña';
          }
          return null;
        },
      ),
    );
  }
}
