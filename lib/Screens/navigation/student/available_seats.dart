import 'package:flutter/material.dart';
import 'package:myapp/HttpService/services.dart';
import 'package:http/http.dart' as http;
import 'package:myapp/models/models.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../../constants.dart';

class AvailableSeats extends StatefulWidget {
  final User user;
  AvailableSeats({Key key, @required this.user}) : super(key: key);

  @override
  _AvailableSeats createState() => _AvailableSeats();
}

String dropdownValue = '0';

http.Response apiResponse;

class _AvailableSeats extends State<AvailableSeats> {
  static User actualUser;
  @override
  void initState() {
    actualUser = widget.user;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(FontAwesomeIcons.chevronLeft,
                    color: appPrimaryColor),
                onPressed: () {
                  Navigator.pop(context);
                },
              );
            },
          ),
          title: Text(
            'Sillas libres biblioteca',
            style: TextStyle(
              color: appPrimaryColor,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
          backgroundColor: Colors.white,
        ),
        body: Center(
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                MyStatefulWidget(),
                ElevatedButton(
                  onPressed: () async {
                    apiResponse = await HttpService().getAvailableChairs(
                        "${actualUser.token}", dropdownValue);
                    _showcontent();
                  },
                  child: Text("Ver disponibilidad"),
                )
              ]),
        ));
  }

  void _showcontent() {
    showDialog(
      context: context, barrierDismissible: false, // user must tap button!

      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('El numero de sillas disponibles es: ' +
              apiResponse.body.toString().substring(11, 12)),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: [
                new Text('Pulsa Ok para cerrar'),
              ],
            ),
          ),
          actions: [
            new FlatButton(
              child: new Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key, List<Widget> children}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      icon: Icon(Icons.arrow_downward),
      iconSize: 24,
      elevation: 16,
      style: TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (String newValue) {
        setState(() {
          dropdownValue = newValue;
        });
      },
      items: <String>[
        '0',
        '1',
        '2',
      ].map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
