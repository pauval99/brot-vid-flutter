import 'package:flutter/material.dart';

import 'package:myapp/screens/home/components/widget_builders.dart';
import '../../constants.dart';
import '../../models/models.dart';

class Home extends StatefulWidget {
  final User user;

  const Home({Key key, this.user}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _index = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        minimum: const EdgeInsets.only(bottom: 5, top: 5),
        child: Center(
          child: Column(
            children: <Widget>[
              _index == 0
                  ? HomeBuilder(
                      widget: widget, context: context, user: widget.user)
                  : (_index == 1
                      ? ProfileBuilder(context: context)
                      : _index == 2
                          ? NotificationsBuilder(
                              context: context,
                              user: widget.user,
                            )
                          : SettingsBuilder(context: context)),
              Padding(
                padding: EdgeInsets.only(
                    top: 5.5,
                    left: 1,
                    right: 1,
                    bottom: MediaQuery.of(context).padding.bottom),
                child: Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          _index = 0;
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: _index == 0
                              ? appSecondaryColor
                              : Colors.transparent,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.home,
                                color: _index == 0
                                    ? appPrimaryColor
                                    : appSecondaryColor,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: Text(_index == 0 ? "Inicio" : "",
                                    style: TextStyle(
                                      color: _index == 0
                                          ? appPrimaryColor
                                          : appSecondaryColor,
                                    )),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          _index = 2;
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: _index == 2
                              ? appSecondaryColor
                              : Colors.transparent,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.notifications,
                                color: _index == 2
                                    ? appPrimaryColor
                                    : appSecondaryColor,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: Text(_index == 2 ? "Notificaciones" : "",
                                    style: TextStyle(
                                      color: _index == 2
                                          ? appPrimaryColor
                                          : appSecondaryColor,
                                    )),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          _index = 3;
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: _index == 3
                              ? appSecondaryColor
                              : Colors.transparent,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.settings,
                                color: _index == 3
                                    ? appPrimaryColor
                                    : appSecondaryColor,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: Text(_index == 3 ? "Ajustes" : "",
                                    style: TextStyle(
                                      color: _index == 3
                                          ? appPrimaryColor
                                          : appSecondaryColor,
                                    )),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
