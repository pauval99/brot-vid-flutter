import 'package:flutter/material.dart';
import '../../constants.dart';
import 'text_field_container.dart';

class RoundedPaswordField extends StatelessWidget {
  final ValueChanged<String> onChanged;
  const RoundedPaswordField({
    Key key,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        obscureText: true,
        onChanged: onChanged,
        decoration: InputDecoration(
          hintText: "Contraseña",
          icon: Icon(
            Icons.lock,
            color: appSecondaryColor,
          ),
          suffixIcon: Icon(Icons.visibility),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
