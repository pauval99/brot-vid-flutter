import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:myapp/blocs/blocs.dart';

import '../../../constants.dart';

class SettingsBuilder extends StatelessWidget {
  const SettingsBuilder({
    Key key,
    @required this.context,
  }) : super(key: key);

  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final authBloc = BlocProvider.of<AuthenticationBloc>(context);
    Size size = MediaQuery.of(context).size;
    return Expanded(
      child: Column(
        children: <Widget>[
          SizedBox(height: 10.0),
          Container(
            margin: EdgeInsets.only(right: 10, left: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Ajustes',
                  style: TextStyle(
                    color: appPrimaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 27,
                  ),
                ),
                IconButton(
                  icon: new Icon(
                    FontAwesomeIcons.signOutAlt,
                    color: appSecondaryColor,
                    size: 23.0,
                  ),
                  onPressed: () {
                    authBloc.add(UserLoggedOut());
                  },
                ),
              ],
            ),
          ),
          SizedBox(height: size.height / 5),
          Image.asset(
            'assets/images/LOGO.png',
            width: 180,
            height: 180,
          ),
          Center(
            child: Text(
              "v.1.0.0",
              style: TextStyle(fontSize: 20.0),
            ),
          )
        ],
      ),
    );
  }
}
