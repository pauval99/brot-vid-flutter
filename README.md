# Brot Vid Flutter

Primero de todo necesitamos tener instalado Flutter en el ordenador, dentro de la documentación se explica muy bien el paso a paso para llevarlo a cabo:

* https://flutter.dev/docs/get-started/install

Una vez instalado Flutter en el PC, podemos ejecutar cualquier tipo de proyecto en un emulador o directamente en un dispositivo móvil. 
Antes de proceder debemos preparar nuestro editor de código e instalar un emulador. En el siguiente enlace podemos encontrar información más en detalle:

* https://flutter.dev/docs/get-started/editor?tab=vscode

Para finalizar descargamos el repositorio y una vez lo tenemos descargado en local, lo abrimos en nuestro editor, y desde el terminal ejecutamos los siguientes comandos (recordar encender antes el emulador) :

* $ flutter doctor		-> verificar que todo esté funcionando
* $ flutter pub get		-> cargar todas las dependencias en local
* $ flutter run  		-> ejecutar la aplicación