import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:myapp/blocs/blocs.dart';
import 'package:myapp/httpService/services.dart';
import 'package:myapp/models/models.dart';
import 'package:myapp/screens/home/components/notification_widgets.dart';

import '../../../constants.dart';

class NotificationsBuilder extends StatefulWidget {
  const NotificationsBuilder({
    Key key,
    @required this.context,
    @required this.user,
  }) : super(key: key);

  final User user;
  final BuildContext context;

  @override
  _NotificationsBuilderState createState() => _NotificationsBuilderState();
}

class _NotificationsBuilderState extends State<NotificationsBuilder> {
  Future<List<Post>> notifications;
  HttpService httpService = HttpService();

  @override
  void initState() {
    super.initState();
    notifications = httpService.getMyNotifications(widget.user.token);
  }

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final authBloc = BlocProvider.of<AuthenticationBloc>(context);
    return Expanded(
      child: Column(
        children: <Widget>[
          SizedBox(height: 10.0),
          Container(
            margin: EdgeInsets.only(right: 10, left: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Mis notificaciones',
                  style: TextStyle(
                    color: appPrimaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 27,
                  ),
                ),
                IconButton(
                  icon: new Icon(
                    FontAwesomeIcons.signOutAlt,
                    color: appSecondaryColor,
                    size: 23.0,
                  ),
                  onPressed: () {
                    authBloc.add(UserLoggedOut());
                  },
                ),
              ],
            ),
          ),
          Expanded(
            child: FutureBuilder(
              future: notifications,
              // ignore: missing_return
              builder:
                  // ignore: missing_return
                  (BuildContext context, AsyncSnapshot<List<Post>> snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.active:
                  case ConnectionState.waiting:
                    return Center(child: CircularProgressIndicator());
                  case ConnectionState.done:
                    if (snapshot.hasError)
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child: Text('Error: ${snapshot.error}'),
                          ),
                        ],
                      );
                     snapshot.data.sort((postA, postB) =>
                        postB.creationDate.compareTo(postA.creationDate));
                     snapshot.data.sort((postA, postB) =>
                        postB.creationDate.compareTo(postA.creationDate));
                    return Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                      child: ListView(
                        children: <Widget>[
                          for (Post myNotification in snapshot.data)
                            if (myNotification.type != 'NOTICIAS')
                              PostWidget(post: myNotification),
                        ],
                      ),
                    );
                }
              },
            ),
          )
        ],
      ),
    );
  }
}
