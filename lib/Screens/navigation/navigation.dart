//common users screens
export 'user_state.dart';
export 'incidencias_form.dart';

//only teacher screens
export 'teacher/students_list.dart';
export 'teacher/student_history.dart';
export 'teacher/subject_list.dart';
export 'teacher/studentID_classes.dart';

//only student screens
export 'student/scan_qr.dart';
export 'student/available_rooms.dart';
export 'student/available_seats.dart';
export 'student/normativa_download.dart';
export 'student/student_classes.dart';
export 'student/biblio_options.dart';
export 'student/more_options.dart';
