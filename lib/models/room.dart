import 'package:equatable/equatable.dart';

class Room extends Equatable {
  final String id;
  final int floor;
  final int number;
  final int capacity;
  final bool closed;
  final String building;
  final bool available;
  final bool pcs;

  const Room({
    this.id,
    this.floor,
    this.number,
    this.capacity,
    this.closed,
    this.building,
    this.available,
    this.pcs,
  });

  @override
  List<Object> get props =>
      [id, floor, number, capacity, closed, building, available, pcs];

  @override
  String toString() => 'Rooms { id: $id}';
}
