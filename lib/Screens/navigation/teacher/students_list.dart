import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:myapp/httpService/services.dart';
import 'package:myapp/models/models.dart';

import '../../../constants.dart';
import '../navigation.dart';

class StudentList extends StatefulWidget {
  final User user;

  const StudentList({Key key, this.user}) : super(key: key);

  @override
  _StudentListState createState() => _StudentListState();
}

class _StudentListState extends State<StudentList> {
  Future<List<User>> futureStudents;
  HttpService httpService = HttpService();
  User currentUser;
  String currentToken;

  @override
  void initState() {
    super.initState();
    futureStudents = httpService.getAllStudents(widget.user.token);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 6),
              child: IconButton(
                icon: const Icon(
                  FontAwesomeIcons.angleLeft,
                  color: appSecondaryColor,
                  size: 30.0,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            );
          },
        ),
        title: Text(
          'Lista de estudiantes',
          style: TextStyle(
            color: appPrimaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 22,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      endDrawer: Padding(
        padding: const EdgeInsets.only(left: 120.0),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25),
            bottomLeft: Radius.circular(25),
          ),
          child: Drawer(
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                Container(
                  height: size.height / 3,
                  child: DrawerHeader(
                    margin: EdgeInsets.zero,
                    padding: EdgeInsets.zero,
                    decoration: BoxDecoration(color: appThirdColor),
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          bottom: 12.0,
                          left: 16.0,
                          child: Text(
                            "Elige tu opción:",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                ListTile(
                  leading: Icon(FontAwesomeIcons.history, size: 22.0),
                  title: Text('Historial de asientos'),
                  onTap: () {
                    if (currentToken == null) {
                      return buildShowDialog(context);
                    }
                    Navigator.push(
                      context,
                      PageRouteBuilder(
                        pageBuilder: (c, a1, a2) => StudentHistory(
                          user: currentUser,
                          token: currentToken,
                        ),
                        transitionsBuilder: (c, anim, a2, child) =>
                            FadeTransition(opacity: anim, child: child),
                        transitionDuration: Duration(milliseconds: 500),
                      ),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(Icons.class__rounded),
                  title: Text('Asignaturas'),
                  onTap: () {
                    if (currentToken == null) {
                      return buildShowDialog(context);
                    }
                    Navigator.push(
                      context,
                      PageRouteBuilder(
                        pageBuilder: (c, a1, a2) => StudentIDClasses(
                          user: currentUser,
                          token: currentToken,
                        ),
                        transitionsBuilder: (c, anim, a2, child) =>
                            FadeTransition(opacity: anim, child: child),
                        transitionDuration: Duration(milliseconds: 500),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
      body: _body(),
    );
  }

  Future buildShowDialog(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alerta !"),
          content: new Text("Debes seleccionar un usuario"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Cerrar"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _body() {
    return FutureBuilder(
      future: futureStudents,
      // ignore: missing_return
      builder: (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            if (snapshot.hasError)
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Text('Error: ${snapshot.error}'),
                  ),
                ],
              );

            return Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.0),
              child: ListView(
                children: <Widget>[
                  for (User user in snapshot.data)
                    if (user.role == 'Alumno')
                      _itemStudent(user, widget.user.token, context),
                ],
              ),
            );
        }
      },
    );
  }

  Widget _itemStudent(User user, String token, BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          currentUser = user;
          currentToken = token;
        });
        Scaffold.of(context).openEndDrawer();
      },
      child: Card(
        elevation: 10.0,
        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: ListTile(
            title: Row(
              children: <Widget>[
                Container(
                  width: 55.0,
                  height: 55.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(55 / 2),
                    image: DecorationImage(
                      image: AssetImage('assets/icons/profile.png'),
                    ),
                  ),
                ),
                SizedBox(width: 25.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      user.name,
                      style: TextStyle(fontSize: 17.0),
                    ),
                    SizedBox(height: 5.0),
                    Row(
                      children: [
                        Container(
                          child: Icon(
                            FontAwesomeIcons.solidEnvelope,
                            color: Colors.grey,
                            size: 14.0,
                          ),
                        ),
                        SizedBox(width: 7.0),
                        Text(
                          user.email,
                          style: TextStyle(
                            fontSize: 15.0,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 5.0),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
