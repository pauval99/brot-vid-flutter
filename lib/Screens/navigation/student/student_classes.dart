import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:myapp/helpers/helpers.dart';
import 'package:myapp/httpService/services.dart';
import 'package:myapp/models/models.dart';

import '../../../constants.dart';

class StudentClasses extends StatefulWidget {
  final User user;

  const StudentClasses({Key key, @required this.user}) : super(key: key);

  @override
  _StudentClassesState createState() => _StudentClassesState();
}

class _StudentClassesState extends State<StudentClasses> {
  Future<List<Class>> futureClasses;
  HttpService httpService = HttpService();

  @override
  void initState() {
    super.initState();
    futureClasses = httpService.getUserClasses(widget.user.token);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 6),
              child: IconButton(
                icon: const Icon(
                  FontAwesomeIcons.angleLeft,
                  color: appSecondaryColor,
                  size: 30.0,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            );
          },
        ),
        title: Text(
          'Mis clases',
          style: TextStyle(
            color: appPrimaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 22,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return FutureBuilder(
      future: futureClasses,
      // ignore: missing_return
      builder: (BuildContext context, AsyncSnapshot<List<Class>> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');

            if (snapshot.data.isNotEmpty)
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: ListView(
                  children: <Widget>[
                    for (Class upcClass in snapshot.data) _seatCard(upcClass),
                  ],
                ),
              );
            else
              return Center(
                child: Text(
                  "El usuario " + widget.user.name + " no tiene clases aún",
                  style: TextStyle(
                    color: appSecondaryColor,
                    fontSize: 20.0,
                  ),
                ),
              );
        }
      },
    );
  }

  Widget _seatCard(Class upcClass) {
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (context) {
            return Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16.0),
              ),
              elevation: 0,
              backgroundColor: Colors.transparent,
              child: dialogContent(context, upcClass),
            );
          },
        );
      },
      child: Card(
        child: ListTile(
          title: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Column(
                  children: <Widget>[
                    Text(
                      upcClass.subject,
                      style: TextStyle(
                        fontSize: 23.0,
                        fontWeight: FontWeight.bold,
                        color: appThirdColor,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 15.0),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            cardRow("Aula: ", upcClass.room),
                            cardRow("Profesor: ", upcClass.teacher),
                            cardRow("Asistencia: ",
                                upcClass.attendance ? 'Presencial' : 'Online'),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Row cardRow(String title, String text) {
    return Row(
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: appSecondaryColor,
          ),
        ),
        Text(
          text,
          style: TextStyle(
            color: appSecondaryColor,
          ),
        ),
      ],
    );
  }

  dialogContent(BuildContext context, Class upcClass) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: 25, bottom: 35, left: 20, right: 20),
          decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(17),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 20.0,
                offset: Offset(10.0, 20.0),
              ),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Align(
                alignment: Alignment.center,
                child: Text(
                  upcClass.subject,
                  style: TextStyle(
                    fontSize: 25.0,
                    color: appThirdColor,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              SizedBox(height: 15.0),
              rowGenerator("Profesor: ", upcClass.teacher, false),
              rowGenerator("Mail: ", upcClass.mail_teacher, false),
              rowGenerator("Clase: ", upcClass.room, false),
              SizedBox(height: 15.0),
              rowGenerator("Día: ",
                  ConversionHelper().formatDay(upcClass.weekday), false),
              rowGenerator(
                  "Inicio: ", upcClass.h_ini.toString() + ":00", false),
              rowGenerator("Final: ", upcClass.h_end.toString() + ":00", false),
              SizedBox(height: 15.0),
              rowGenerator("Asistencia: ",
                  upcClass.attendance ? 'Presencial' : 'Online', true),
              rowGenerator(
                  "Infectados: ", upcClass.num_infected.toString(), true),
            ],
          ),
        ),
      ],
    );
  }

  Row rowGenerator(String title, String content, bool confined) {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 1.0),
          child: Text(
            title,
            style: TextStyle(
              fontSize: 17.0,
              fontWeight: FontWeight.w700,
              color: confined ? appThirdColor : myBlue,
            ),
          ),
        ),
        Text(
          content,
          style: TextStyle(
            fontSize: 16.0,
            color: appSecondaryColor,
          ),
        ),
      ],
    );
  }
}
