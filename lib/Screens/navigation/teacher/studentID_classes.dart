import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:myapp/httpService/services.dart';
import 'package:myapp/models/models.dart';
import 'package:myapp/screens/global_widgets/settings_item.dart';

import '../../../constants.dart';

class StudentIDClasses extends StatefulWidget {
  final User user;
  final String token;

  const StudentIDClasses({Key key, @required this.user, @required this.token})
      : super(key: key);

  @override
  _StudentIDClassesState createState() => _StudentIDClassesState();
}

class _StudentIDClassesState extends State<StudentIDClasses> {
  Future<List<Class>> futureClasses;
  HttpService httpService = HttpService();

  @override
  void initState() {
    super.initState();
    futureClasses =
        httpService.getUserClassesByID(widget.token, widget.user.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 6),
              child: IconButton(
                icon: const Icon(
                  FontAwesomeIcons.angleLeft,
                  color: appSecondaryColor,
                  size: 30.0,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            );
          },
        ),
        title: Text(
          'Asignaturas',
          style: TextStyle(
            color: appPrimaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 22,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return FutureBuilder(
      future: futureClasses,
      // ignore: missing_return
      builder: (BuildContext context, AsyncSnapshot<List<Class>> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');

            if (snapshot.data.isNotEmpty)
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Column(
                  children: <Widget>[
                    for (Class upcClass in snapshot.data)
                      SettingsItem(
                          Icons.class__rounded,
                          appThirdColor,
                          upcClass.subject,
                          upcClass.attendance ? 'Presencial' : 'Online'),
                  ],
                ),
              );
            else
              return Center(
                child: Text(
                  "El usuario " + widget.user.name + " no tiene clases aún",
                  style: TextStyle(
                    color: appSecondaryColor,
                    fontSize: 20.0,
                  ),
                ),
              );
        }
      },
    );
  }
}
