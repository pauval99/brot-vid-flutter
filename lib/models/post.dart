import 'package:equatable/equatable.dart';

class Post extends Equatable {
  final String title;
  final String description;
  final String type;
  final String site;
  final DateTime creationDate;

  const Post({
    this.title,
    this.description,
    this.type,
    this.site,
    this.creationDate,
  });

  @override
  List<Object> get props => [title, description, type, site, creationDate];

  @override
  String toString() =>
      'Notification { title: $title, description: $description, type: $type, site: $site, creationDate: $creationDate }';
}
