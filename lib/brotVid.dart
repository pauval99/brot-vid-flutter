import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'screens/screens.dart';
import 'blocs/authentication/authentication.dart';
import 'constants.dart';

class BrotVid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'BrotVid Demo',
      theme: ThemeData(
        primarySwatch: myPrimaryColor,
        scaffoldBackgroundColor: Colors.white,
      ),
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          if (state is AuthenticationAuthenticated)
            return Home(user: state.user);
          return LoginPage();
        },
      ),
    );
  }
}

class BrotVidSplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Splash Screen'),
      ),
    );
  }
}
