import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:myapp/simple_bloc_observer.dart';
import 'package:myapp/httpService/services.dart';
import 'package:myapp/blocs/blocs.dart';
import 'package:myapp/brotVid.dart';

void main() {
  Bloc.observer = SimpleBlocObserver();
  return runApp(
    RepositoryProvider<AuthenticationService>(
      create: (context) => HttpAuthenticationService(),
      child: BlocProvider<AuthenticationBloc>(
        create: (context) {
          final authenticationService =
              RepositoryProvider.of<AuthenticationService>(context);
          return AuthenticationBloc(authenticationService)..add(AppLoaded());
        },
        child: BrotVid(),
      ),
    ),
  );
}
