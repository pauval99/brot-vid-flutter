import 'package:flutter/material.dart';
import 'package:myapp/httpService/services.dart';

import 'package:myapp/models/models.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../../constants.dart';

class AvailableRooms extends StatefulWidget {
  final User user;
  AvailableRooms({Key key, @required this.user}) : super(key: key);

  @override
  _AvailableRooms createState() => _AvailableRooms();
}

class _AvailableRooms extends State<AvailableRooms> {
  List<Room> _rooms = List<Room>();

  @override
  void initState() {
    _fetchRooms(widget.user.token).then((value) {
      setState(() {
        for (Room each in value) {
          if (each.building != "BIBLIOTECA") _rooms.add(each);
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 6),
              child: IconButton(
                icon: const Icon(
                  FontAwesomeIcons.angleLeft,
                  color: appSecondaryColor,
                  size: 30.0,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            );
          },
        ),
        title: Text(
          'Aulas disponibles',
          style: TextStyle(
            color: appPrimaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 22,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: ListView.builder(
        itemCount: _rooms.length,
        itemBuilder: (context, index) {
          return Card(
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 32.0, bottom: 32.0, left: 16.0, right: 16.0),
              child: Column(
                children: <Widget>[
                  Visibility(
                    visible: _rooms[index].available,
                    child: Text(
                      _rooms[index].building +
                          _rooms[index].floor.toString() +
                          "0" +
                          _rooms[index].number.toString(),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Future<List<Room>> _fetchRooms(String token) async {
    return HttpService().getAvailableRooms(token);
  }
}
