import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:myapp/httpService/services.dart';
import 'package:myapp/models/models.dart';

import '../../../constants.dart';

class NormativaDownload extends StatefulWidget {
  final User user;

  const NormativaDownload({Key key, this.user}) : super(key: key);

  @override
  _NormativaDownloadState createState() => _NormativaDownloadState();
}

class _NormativaDownloadState extends State<NormativaDownload> {
  List<Post> normativas = List<Post>();
  HttpService httpService = HttpService();

  @override
  void initState() {
    _fetchNotifications(widget.user.token).then((value) {
      setState(() {
        for (Post normativa in value) {
          if (normativa.type == 'NORMATIVA') normativas.add(normativa);
        }
        normativas.sort(
            (postA, postB) => postB.creationDate.compareTo(postA.creationDate));
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 6),
              child: IconButton(
                icon: const Icon(
                  FontAwesomeIcons.angleLeft,
                  color: appSecondaryColor,
                  size: 30.0,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            );
          },
        ),
        title: Text(
          'Cambios en la normativa',
          style: TextStyle(
            color: appPrimaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 22,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return ListView.builder(
      itemCount: normativas.length,
      itemBuilder: (context, index) {
        return _getCardNormativa(normativas[index]);
      },
    );
  }

  Widget _getCardNormativa(Post post) {
    return Card(
      elevation: 1.0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            title: Text(
              post.title,
              style: TextStyle(
                color: appPrimaryColor,
                fontWeight: FontWeight.bold,
                fontSize: 14.0,
              ),
            ),
            isThreeLine: true,
            subtitle: Padding(
              padding: const EdgeInsets.only(top: 6.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    post.description,
                    style: TextStyle(
                      fontSize: 14.0,
                      color: myBlue,
                    ),
                  ),
                ],
              ),
            ),
            dense: true,
          ),
        ],
      ),
    );
  }

  Future<List<Post>> _fetchNotifications(String token) async {
    return HttpService().getMyNotifications(token);
  }
}
