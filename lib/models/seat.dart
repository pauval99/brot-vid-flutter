import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class Seat extends Equatable {
  final String teacher;
  final int floor;
  final int number;
  final String building;
  // ignore: non_constant_identifier_names
  final int h_ini;
  // ignore: non_constant_identifier_names
  final int h_end;
  final String day;
  final String subject;
  final String chair;
  final DateTime date;

  const Seat({
    @required this.teacher,
    @required this.floor,
    @required this.number,
    @required this.building,
    // ignore: non_constant_identifier_names
    @required this.h_ini,
    // ignore: non_constant_identifier_names
    @required this.h_end,
    @required this.day,
    @required this.subject,
    @required this.chair,
    @required this.date,
  });

  @override
  List<Object> get props =>
      [teacher, floor, number, building, h_ini, h_end, day, date];

  @override
  String toString() => 'Seat { title: $teacher, chair: $chair, date: $date }';
}
