import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:myapp/httpService/services.dart';
import 'package:myapp/models/user.dart';

import '../../constants.dart';

final HttpService httpService = HttpService();

class UserState extends StatefulWidget {
  final String token;

  const UserState({@required this.token});
  @override
  _UserStateState createState() => _UserStateState();
}

class _UserStateState extends State<UserState> {
  User actualUser = User(
    confined: false,
    email: null,
    infected: false,
    name: null,
    role: null,
    signupDate: null,
  );
  bool infected = false;
  bool confined = false;

  @override
  void initState() {
    super.initState();
    _getActualUser(widget.token).then((value) {
      setState(() {
        actualUser = value;
        infected = actualUser.infected;
        confined = actualUser.confined;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(FontAwesomeIcons.chevronLeft,
                  color: appPrimaryColor),
              onPressed: () {
                Navigator.pop(context);
              },
            );
          },
        ),
        title: Text(
          'Estado de salud',
          style: TextStyle(
            color: appPrimaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 75.0),
            _buildChild(),
            SizedBox(height: 25.0),
            Visibility(
                visible: !infected,
                child: ElevatedButton(
                  onPressed: () async {
                    await httpService.addPositive("${actualUser.token}");
                    _showcontentp();
                  },
                  child: Text('Añadir test positivo'),
                )),
            Visibility(
                visible: infected,
                child: ElevatedButton(
                  onPressed: () async {
                    await httpService.addNegative("${actualUser.token}");
                    _showcontentn();
                  },
                  child: Text('Añadir test negativo'),
                )),
            Visibility(
                visible: !confined,
                child: ElevatedButton(
                  onPressed: () async {
                    await httpService.addConfined("${actualUser.token}");
                    _showcontentc();
                  },
                  child: Text('Cambiar estado a confinado'),
                )),
            Visibility(
                visible: confined,
                child: ElevatedButton(
                  onPressed: () async {
                    await httpService.addDesconfined("${actualUser.token}");
                    _showcontentd();
                  },
                  child: Text('Cambiar estado a desconfinado'),
                )),
          ],
        ),
      ),
    );
  }

  void _showcontentp() {
    showDialog(
      context: context, barrierDismissible: false, // user must tap button!

      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('Añadir positivo correcto'),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: [
                new Text('Se ha añadido el usuario como positivo'),
              ],
            ),
          ),
          actions: [
            new FlatButton(
              child: new Text('Ok'),
              onPressed: () {
                setState(() {
                  infected = true;
                });
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  void _showcontentn() {
    showDialog(
      context: context, barrierDismissible: false, // user must tap button!

      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('Añadir negativo correcto'),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: [
                new Text('Se ha añadido el usuario como negativo'),
              ],
            ),
          ),
          actions: [
            new FlatButton(
              child: new Text('Ok'),
              onPressed: () {
                setState(() {
                  infected = false;
                });
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  void _showcontentc() {
    showDialog(
      context: context, barrierDismissible: false, // user must tap button!

      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('Añadir confinado correcto'),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: [
                new Text('Se ha añadido el usuario como confinado'),
              ],
            ),
          ),
          actions: [
            new FlatButton(
              child: new Text('Ok'),
              onPressed: () {
                setState(() {
                  confined = true;
                });
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  void _showcontentd() {
    showDialog(
      context: context, barrierDismissible: false, // user must tap button!

      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('Añadir desconfinado correcto'),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: [
                new Text('Se ha añadido el usuario como desconfinado'),
              ],
            ),
          ),
          actions: [
            new FlatButton(
              child: new Text('Ok'),
              onPressed: () {
                setState(() {
                  confined = false;
                });
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildChild() {
    if (infected == true && confined == true) {
      return Text(
        'Tu estado es: positivo y confinado ',
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
      );
    } else if (infected == true && confined == false) {
      return Text(
        'Tu estado es: positivo y desconfinado  ',
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
      );
    } else if (infected == false && confined == false) {
      return Text(
        'Tu estado es: negativo y desconfinado  ',
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
      );
    } else
      return Text(
        'Tu estado es: negativo y confinado  ',
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
      );
  }

  Future<User> _getActualUser(String token) async {
    return await HttpService().getUser(token);
  }
}
