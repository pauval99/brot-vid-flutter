import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:myapp/httpService/services.dart';
import 'package:myapp/helpers/helpers.dart';
import 'package:myapp/models/models.dart';
import 'package:http/http.dart' as http;

import '../../constants.dart';

class IncidenciasForm extends StatefulWidget {
  final User user;

  const IncidenciasForm({Key key, @required this.user}) : super(key: key);

  @override
  _IncidenciasFormState createState() => _IncidenciasFormState();
}

class _IncidenciasFormState extends State<IncidenciasForm> {
  final _formKey = GlobalKey<FormState>();
  final _descriptionController = TextEditingController();
  final _titleController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 6),
              child: IconButton(
                icon: const Icon(
                  FontAwesomeIcons.angleLeft,
                  color: appSecondaryColor,
                  size: 30.0,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            );
          },
        ),
        title: Text(
          'Notificar incidencia',
          style: TextStyle(
            color: appPrimaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 22,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: size.height / 6),
                child: Center(
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 50,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 5),
                      child: Icon(
                        FontAwesomeIcons.mapMarkedAlt,
                        size: 100.0,
                        color: appThirdColor,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 60.0),
              buildField('Título de la incidencia',
                  'Asigna un título a tu incidencia', _descriptionController),
              buildField('Descripción de la incidencia',
                  'Agrega una descripción a tu incidencia', _titleController),
              SizedBox(height: 10.0),
              Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: ElevatedButton(
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        http.Response response =
                            await httpService.addIncidences(
                                widget.user.token,
                                _titleController.text,
                                _descriptionController.text);
                        response.statusCode == 201
                            ? DialogHelper().buildShowDialog(
                                context, json.decode(response.body), true)
                            : DialogHelper().buildShowDialog(
                                context, json.decode(response.body), false);
                        setState(() {
                          _titleController.clear();
                          _descriptionController.clear();
                        });
                      }
                    },
                    child: Text('Enviar'),
                  ),
                ),
              ),
              SizedBox(height: 80.0),
            ],
          ),
        ),
      ),
    );
  }

  Padding buildField(
      String labelText, String errorMessage, TextEditingController controller) {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
      child: TextFormField(
        decoration: InputDecoration(
          errorStyle: TextStyle(
            fontSize: 14.0,
            color: appThirdColor,
          ),
          labelText: labelText,
        ),
        controller: controller,
        validator: (value) {
          if (value.isEmpty) {
            return errorMessage;
          }
          return null;
        },
      ),
    );
  }
}
