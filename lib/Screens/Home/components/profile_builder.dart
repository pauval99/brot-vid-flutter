import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:myapp/blocs/blocs.dart';

import '../../../constants.dart';

class ProfileBuilder extends StatelessWidget {
  const ProfileBuilder({
    Key key,
    @required this.context,
  }) : super(key: key);

  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final authBloc = BlocProvider.of<AuthenticationBloc>(context);
    return Expanded(
      child: Column(
        children: <Widget>[
          SizedBox(height: 10.0),
          Container(
            margin: EdgeInsets.only(right: 10, left: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Mi Perfil',
                  style: TextStyle(
                    color: appPrimaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 27,
                  ),
                ),
                IconButton(
                  icon: new Icon(
                    FontAwesomeIcons.signOutAlt,
                    color: appSecondaryColor,
                    size: 23.0,
                  ),
                  onPressed: () {
                    authBloc.add(UserLoggedOut());
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
