import 'dart:convert';
import 'package:myapp/models/models.dart';
import 'package:http/http.dart' as http;

class ConversionHelper {
  DateTime _convertToDatetime(String stringTime) {
    String newTime = stringTime.substring(0, 19);
    return DateTime.parse(newTime);
  }

  User toCurrentUser(http.Response response, String token) {
    var res = jsonDecode(response.body);
    bool confined, infected;
    String role;
    if (res["message"]["confined"].toString() == "true")
      confined = true;
    else
      confined = false;
    if (res["message"]["infected"].toString() == "true")
      infected = true;
    else
      infected = false;
    if (res["message"]["role"].toString() == "ALUMNO")
      role = "Alumno";
    else
      role = "Profesor";
    return User(
      id: res["message"]['_id'],
      name: res["message"]["displayName"].toString(),
      role: role,
      confined: confined,
      email: res["message"]["email"].toString(),
      infected: infected,
      signupDate: res["message"]["signupDate"].toString(),
      token: token,
    );
  }

  User toUserModel(userJson) {
    bool confined, infected;
    String role;
    if (userJson["confined"].toString() == "true")
      confined = true;
    else
      confined = false;
    if (userJson["infected"].toString() == "true")
      infected = true;
    else
      infected = false;
    if (userJson["role"].toString() == "ALUMNO")
      role = "Alumno";
    else
      role = "Profesor";
    return User(
      id: userJson['_id'],
      name: userJson['displayName'],
      role: role,
      email: userJson['email'],
      confined: confined,
      infected: infected,
      signupDate: userJson['signupDate'],
    );
  }

  Subject toSubjectModel(rawPost) {
    String building, atendance;
    if (rawPost['room']['number'].toString().length >= 1)
      building = rawPost['room']['building'] +
          rawPost['room']['floor'].toString() +
          '0' +
          rawPost['room']['number'].toString();
    else
      building = rawPost['room']['building'] +
          rawPost['room']['floor'].toString() +
          rawPost['room']['number'].toString();
    if (rawPost['subject']['attendance'])
      atendance = "Presencial";
    else
      atendance = "Online";
    return Subject(
        name: rawPost['subject']['name'],
        attendance: rawPost['subject']['attendance'],
        presencialidad: atendance,
        code: rawPost['subject']['code'],
        room: building);
  }

  Post toNotificationModel(notificationJson) {
    return Post(
      title: notificationJson['title'],
      description: notificationJson['description'],
      type: notificationJson['type'],
      site: notificationJson['site'],
      creationDate: _convertToDatetime(notificationJson['creationDate']),
    );
  }

  Seat toSeatModel(seatJson) {
    return Seat(
      teacher: seatJson['class']['teacher']['displayName'],
      floor: seatJson['class']['room']['floor'],
      number: seatJson['class']['room']['number'],
      building: seatJson['class']['room']['building'],
      h_ini: seatJson['class']['schedule']['h_ini'],
      h_end: seatJson['class']['schedule']['h_end'],
      day: seatJson['class']['schedule']['day'],
      subject: seatJson['class']['subject']['name'],
      chair: seatJson['chair']['QR'].substring(0, 1),
      date: _convertToDatetime(seatJson['date']),
    );
  }

  Class toClassModel(classJson) {
    return Class(
      subject: classJson['subject'],
      room: classJson['room'],
      weekday: classJson['weekday'],
      h_ini: classJson['h_ini'],
      h_end: classJson['h_end'],
      teacher: classJson['teacher'],
      mail_teacher: classJson['mail_teacher'],
      num_infected: classJson['num_infected'],
      attendance: classJson['attendance'],
    );
  }

  Room toRoomModel(roomJson) {
    return Room(
      id: roomJson['id'],
      floor: roomJson['floor'],
      number: roomJson['number'],
      capacity: roomJson['capacity'],
      closed: roomJson['closed'],
      building: roomJson['building'],
      available: roomJson['available'],
      pcs: roomJson['pcs'],
    );
  }

  String formatDay(String weekday) {
    switch (weekday) {
      case "MONDAY":
        return "Lunes";
        break;
      case "TUESDAY":
        return "Martes";
        break;
      case "WEDNSDAY":
        return "Miércoles";
        break;
      case "THURSDAY":
        return "Jueves";
        break;
      case "FRIDAY":
        return "Viernes";
        break;
      default:
        return "Viernes";
        break;
    }
  }

  String spanishMonth(String month) {
    switch (month) {
      case "January":
        return "enero";
        break;
      case "February":
        return "febrero";
        break;
      case "March":
        return "marzo";
        break;
      case "April":
        return "abril";
        break;
      case "May":
        return "mayo";
        break;
      case "June":
        return "junio";
        break;
      case "July":
        return "julio";
        break;
      case "Agost":
        return "agosto";
        break;
      case "September":
        return "septiembre";
        break;
      case "October":
        return "octubre";
        break;
      case "November":
        return "noviembre";
        break;
      case "December":
        return "diciembre";
        break;
      default:
        return "mes";
        break;
    }
  }
}
