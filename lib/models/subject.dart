import 'package:equatable/equatable.dart';

class Subject extends Equatable {
  final String id;
  final int code;
  final String name;
  final bool attendance;
  final String presencialidad;
  final String room;

  const Subject({
    this.id,
    this.code,
    this.name,
    this.attendance,
    this.presencialidad,
    this.room,
  });

  @override
  List<Object> get props => [id, code, name, attendance, presencialidad, room];

  @override
  String toString() =>
      'Subject { id: $id, name: $name, code: $code, presencialidad: $presencialidad, room: $room}';
}
