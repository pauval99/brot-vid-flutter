import 'package:ext_storage/ext_storage.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:division/division.dart';
import 'package:myapp/models/models.dart';
import 'dart:io';

import '../../../constants.dart';
import '../navigation.dart';

class MoreOptions extends StatefulWidget {
  final User user;

  const MoreOptions({Key key, @required this.user}) : super(key: key);

  @override
  _MoreOptionsState createState() => _MoreOptionsState();
}

final pdf_url =
    "https://www.upc.edu/es/sala-de-prensa/pdf/protocol-coronavirus-upc-es.pdf";

var dio = Dio();

class _MoreOptionsState extends State<MoreOptions> {
  bool pressed = false;

  void navigation(int navigation) {
    switch (navigation) {
      case 0:
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => AvailableRooms(user: widget.user),
          ),
        );
        break;
      case 1:
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => NormativaDownload(user: widget.user),
          ),
        );
        break;
      case 2:
        beginDownloadFile();
        break;
      default:
        print("Opción inválida");
        break;
    }
  }

  void beginDownloadFile() async {
    String path = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOWNLOADS);
    String fullPath = "$path/manual.pdf";
    download2(dio, pdf_url, fullPath);
    _showcontent();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 6),
              child: IconButton(
                icon: const Icon(
                  FontAwesomeIcons.angleLeft,
                  color: appSecondaryColor,
                  size: 30.0,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            );
          },
        ),
        title: Text(
          'Más opciones',
          style: TextStyle(
            color: appPrimaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 22,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return Column(
      children: <Widget>[
        parentOption(
          0,
          FontAwesomeIcons.mapMarkerAlt,
          "Consultar aulas disponibles",
        ),
        parentOption(
          1,
          FontAwesomeIcons.atlas,
          "Canvios de normativa",
        ),
        parentOption(
          2,
          FontAwesomeIcons.fileDownload,
          "Descargar manual",
        ),
      ],
    );
  }

  Parent parentOption(int index, IconData icon, String text) {
    return Parent(
      style: settingsItemStyle(pressed),
      gesture: Gestures()
        ..onTap(() => navigation(index))
        ..isTap((isTapped) => setState(() => pressed = isTapped)),
      child: Row(
        children: <Widget>[
          Parent(
            style: settingsItemIconStyle(appThirdColor),
            child: Icon(icon, color: Colors.white, size: 22),
          ),
          SizedBox(width: 15),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Txt(text, style: itemTitleTextStyle),
            ],
          )
        ],
      ),
    );
  }

  final settingsItemStyle = (pressed) => ParentStyle()
    ..elevation(pressed ? 0 : 50, color: Colors.grey)
    ..scale(pressed ? 0.95 : 1.0)
    ..alignmentContent.center()
    ..height(70)
    ..margin(vertical: 5)
    ..borderRadius(all: 15)
    ..background.hex('#ffffff')
    ..ripple(true)
    ..animate(150, Curves.easeOut);

  final settingsItemIconStyle = (Color color) => ParentStyle()
    ..background.color(color)
    ..margin(left: 20)
    ..padding(all: 15)
    ..borderRadius(all: 30);

  final TxtStyle itemTitleTextStyle = TxtStyle()
    ..bold()
    ..fontSize(18);

  @override
  void initState() {
    getPermission();
  }

  void getPermission() async {
    print("getPermission");
    await PermissionHandler().requestPermissions([PermissionGroup.storage]);
  }

  Future download2(Dio dio, String url, String savePath) async {
    try {
      Response response = await dio.get(
        url,
        onReceiveProgress: showDownloadProgress,
        options: Options(
            responseType: ResponseType.bytes,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            }),
      );

      File file = File(savePath);
      var raf = file.openSync(mode: FileMode.write);
      raf.writeFromSync(response.data);
      await raf.close();
    } catch (e) {
      print("el error es");
      print(e);
    }
  }

  void showDownloadProgress(received, total) {
    if (total != -1) {
      print((received / total * 100).toStringAsFixed(0) + "%");
    }
  }

  void _showcontent() {
    showDialog(
      context: context, barrierDismissible: false, // user must tap button!

      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('Descarga completada correctamente'),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: [
                new Text('Pulsa Ok para cerrar'),
              ],
            ),
          ),
          actions: [
            new FlatButton(
              child: new Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
