import 'dart:async';

import 'package:meta/meta.dart';
import 'package:myapp/httpService/services.dart';
import 'package:myapp/models/models.dart';
import 'package:rxdart/rxdart.dart';
import 'package:bloc/bloc.dart';
import 'notification.dart';

class PostBloc extends Bloc<PostEvent, PostState> {
  final HttpService httpClient;
  int perPage = 10;
  int present = 0;
  bool finish = false;
  List<Post> originalItems;
  List<Post> items = List<Post>();

  PostBloc({@required this.httpClient}) : super(PostInitial());

  @override
  Stream<Transition<PostEvent, PostState>> transformEvents(
    Stream<PostEvent> events,
    TransitionFunction<PostEvent, PostState> transitionFn,
  ) {
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<PostState> mapEventToState(PostEvent event) async* {
    final currentState = state;
    if (event is PostFetched && !_hasReachedMax(currentState)) {
      try {
        if (currentState is PostInitial) {
          final posts = await _fetchPosts();
          if (posts.isEmpty) yield PostSuccess(posts: items);
          originalItems = posts;
          originalItems.sort((postA, postB) =>
              postB.creationDate.compareTo(postA.creationDate));
          if ((present + perPage) > originalItems.length) {
            items.addAll(originalItems.getRange(present, originalItems.length));
            yield PostSuccess(posts: items, hasReachedMax: true);
          } else if ((present + perPage) <= originalItems.length) {
            items.addAll(originalItems.getRange(present, present + perPage));
            yield PostSuccess(posts: items, hasReachedMax: false);
          }
          return;
        }
        if (currentState is PostSuccess) {
          items.clear();
          if ((present + perPage) > originalItems.length) {
            items.addAll(originalItems.getRange(present, originalItems.length));
            finish = true;
          } else if ((present + perPage) <= originalItems.length) {
            items.addAll(originalItems.getRange(present, present + perPage));
          }
          present = present + perPage;

          if (items.isEmpty) {
            yield currentState.copyWith(hasReachedMax: true);
          } else if (finish == true) {
            yield PostSuccess(
              posts: currentState.posts + items,
              hasReachedMax: true,
            );
          } else
            yield PostSuccess(
              posts: currentState.posts + items,
              hasReachedMax: false,
            );
        }
      } catch (_) {
        yield PostFailure();
      }
    }
  }

  bool _hasReachedMax(PostState state) =>
      state is PostSuccess && state.hasReachedMax;

  Future<List<Post>> _fetchPosts() async {
    final String token = await SharedPreferencesHelper.getToken();
    return httpClient.getMyNotifications(token);
  }
}
