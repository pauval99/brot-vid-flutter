import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:division/division.dart';
import 'package:myapp/models/models.dart';

import '../../../constants.dart';
import '../navigation.dart';

class BiblioOptions extends StatefulWidget {
  final User user;

  const BiblioOptions({Key key, @required this.user}) : super(key: key);

  @override
  _BiblioOptionsState createState() => _BiblioOptionsState();
}

class _BiblioOptionsState extends State<BiblioOptions> {
  bool pressed = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 6),
              child: IconButton(
                icon: const Icon(
                  FontAwesomeIcons.angleLeft,
                  color: appSecondaryColor,
                  size: 30.0,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            );
          },
        ),
        title: Text(
          'Biblioteca',
          style: TextStyle(
            color: appPrimaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 22,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return Column(
      children: <Widget>[
        Parent(
          style: settingsItemStyle(pressed),
          gesture: Gestures()
            ..onTap(
              () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AvailableSeats(user: widget.user),
                ),
              ),
            )
            ..isTap((isTapped) => setState(() => pressed = isTapped)),
          child: Row(
            children: <Widget>[
              Parent(
                style: settingsItemIconStyle(appSecondaryColor),
                child: Icon(FontAwesomeIcons.locationArrow,
                    color: appPrimaryColor, size: 22),
              ),
              SizedBox(width: 15),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Txt("Buscar sitios libres", style: itemTitleTextStyle),
                ],
              )
            ],
          ),
        ),
        Parent(
          style: settingsItemStyle(pressed),
          gesture: Gestures()
            ..onTap(
              () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      ScanQR(user: widget.user, biblioteca: true),
                ),
              ),
            )
            ..isTap((isTapped) => setState(() => pressed = isTapped)),
          child: Row(
            children: <Widget>[
              Parent(
                style: settingsItemIconStyle(appSecondaryColor),
                child: Icon(FontAwesomeIcons.qrcode,
                    color: appPrimaryColor, size: 22),
              ),
              SizedBox(width: 15),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Txt("Reservar sitio en biblioteca",
                      style: itemTitleTextStyle),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }

  final settingsItemStyle = (pressed) => ParentStyle()
    ..elevation(pressed ? 0 : 50, color: Colors.grey)
    ..scale(pressed ? 0.95 : 1.0)
    ..alignmentContent.center()
    ..height(70)
    ..margin(vertical: 5)
    ..borderRadius(all: 15)
    ..background.hex('#ffffff')
    ..ripple(true)
    ..animate(150, Curves.easeOut);

  final settingsItemIconStyle = (Color color) => ParentStyle()
    ..background.color(color)
    ..margin(left: 20)
    ..padding(all: 15)
    ..borderRadius(all: 30);

  final TxtStyle itemTitleTextStyle = TxtStyle()
    ..bold()
    ..fontSize(18);
}
