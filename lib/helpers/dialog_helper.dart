import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../constants.dart';

class DialogHelper {
  Future buildShowDialog(BuildContext context, datajson, bool success) {
    return showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
          child: dialogContent(context, datajson["message"], success),
        );
      },
    );
  }

  dialogContent(BuildContext context, String message, bool success) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: 85, bottom: 16, left: 16, right: 16),
          margin: EdgeInsets.only(top: 37),
          decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(17),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: Offset(10.0, 20.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                success ? "Acción exitosa !" : "Lo sentimos !",
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
              SizedBox(height: 24.0),
              Center(
                child: Text(
                  message.toUpperCase(),
                  style: TextStyle(fontSize: 16.0),
                ),
              ),
              SizedBox(height: 24.0),
              Align(
                alignment: Alignment.bottomRight,
                child: FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Confirmar"),
                ),
              )
            ],
          ),
        ),
        Positioned(
          top: 0,
          left: 16.0,
          right: 16.0,
          child: CircleAvatar(
            backgroundColor: newBackground,
            radius: 50,
            child: Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Icon(
                success ? FontAwesomeIcons.checkDouble : FontAwesomeIcons.bomb,
                size: 45.0,
                color: success ? myGreen : myRed,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
