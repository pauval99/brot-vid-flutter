import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  static final String token = "token";

  static Future<bool> setToken(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(token, value);
  }

  static Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(token) ?? 'null';
  }

  static deleteCredentials() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(token);
  }
}
