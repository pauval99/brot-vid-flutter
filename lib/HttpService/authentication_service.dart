import '../exceptions/exceptions.dart';
import '../models/models.dart';
import 'services.dart';

final HttpService httpService = HttpService();

abstract class AuthenticationService {
  Future<User> getCurrentUser();
  Future<User> signInWithEmailAndPassword(String email, String password);
  Future<void> signOut();
}

class HttpAuthenticationService extends AuthenticationService {
  @override
  Future<User> getCurrentUser() async {
    String token = await SharedPreferencesHelper.getToken();
    if (token == 'null') return null;
    return await httpService.getUser(token);
  }

  @override
  Future<User> signInWithEmailAndPassword(String email, String password) async {
    await Future.delayed(Duration(seconds: 1));

    if (await httpService.signIn(email.toLowerCase(), password) == 404) {
      throw AuthenticationException(message: 'Email o contraseña no válidas');
    } else if (await httpService.signIn(email.toLowerCase(), password) == 400) {
      throw AuthenticationException(message: 'Contraseña incorrecta');
    } else {
      String token = await SharedPreferencesHelper.getToken();
      return await httpService.getUser(token);
    }
  }

  @override
  Future<void> signOut() async {
    return await httpService.logOut();
  }
}
